package com.kevalpatel2106.navigation


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.navigation.fragment.NavHostFragment.findNavController
import kotlinx.android.synthetic.main.fragment_android.*

/**
 * A simple [Fragment] subclass.
 *
 */
class AndroidFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_android, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        start_android_quiz_btn.setOnClickListener {
            findNavController(this@AndroidFragment)
                    .navigate(
                            R.id.action_androidFragment_to_quizFragment,
                            QuizFragmentArgs.Builder(0).build().toBundle()
                    )
        }
    }

}
