package com.kevalpatel2106.navigation


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.NavHostFragment.findNavController
import kotlinx.android.synthetic.main.fragment_ios.*

/**
 * A simple [Fragment] subclass.
 *
 */
class IOsFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_ios, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        start_ios_quiz_btn.setOnClickListener {
            findNavController(this@IOsFragment)
                    .navigate(
                            R.id.action_iOsFragment_to_quizFragment,
                            QuizFragmentArgs.Builder(2).build().toBundle()
                    )
        }
    }
}
