package com.kevalpatel2106.navigation

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.navigation.fragment.NavHostFragment.findNavController
import kotlinx.android.synthetic.main.fragment_quiz.*


class QuizFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_quiz, container, false)
    }

    private lateinit var args : QuizFragmentArgs
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        args = QuizFragmentArgs.fromBundle(arguments)

        question_tv.text = when(args.platform){
            0 -> "What is the latest version of android?"
            1 -> "What is the latest version of flutter?"
            2 -> "What is the latest version of iOS?"
            else -> throw IllegalArgumentException("Invalid args.")
        }

        quiz_pass_button.setOnClickListener {
            findNavController(this@QuizFragment)
                    .navigate(R.id.action_quizFragment2_to_successFragment)
        }

        quiz_fail_button.setOnClickListener {
            findNavController(this@QuizFragment)
                    .navigate(R.id.action_quizFragment2_to_errorFragment)
        }
    }
}
