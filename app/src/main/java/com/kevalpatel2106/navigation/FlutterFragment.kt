package com.kevalpatel2106.navigation


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.NavHostFragment
import kotlinx.android.synthetic.main.fragment_flutter.*

/**
 * A simple [Fragment] subclass.
 *
 */
class FlutterFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_flutter, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        start_flutter_quiz_btn.setOnClickListener {
            NavHostFragment.findNavController(this@FlutterFragment)
                    .navigate(
                            R.id.action_flutterFragment_to_quizFragment,
                            QuizFragmentArgs.Builder(1).build().toBundle()
                    )
        }
    }

}
